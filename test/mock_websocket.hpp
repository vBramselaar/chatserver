#ifndef MOCK_WEBSOCKET_HPP
#define MOCK_WEBSOCKET_HPP

#include <gmock/gmock.h>
#include "connection.hpp"
#include <functional>

class mock_websocket : public connection<mock_websocket>
{
public:
	MOCK_METHOD(void, bind_impl, (std::string_view addr, unsigned short port));
	MOCK_METHOD(void, run_impl, ());
	MOCK_METHOD(void, on_accept_impl, (std::function<void()> callback));
};

#endif
