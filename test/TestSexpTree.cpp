#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "s-expression/sexpTree.hpp"
#include "s-expression/sexp.hpp"

TEST(TestSexpTreeParser, BasicToString)
{
	bvd::sexp_p root = bvd::Sexp::make_list({});
	root->add(bvd::Sexp::make_atom("message"));

	std::string t = "lol";
	bvd::sexp_p date = bvd::Sexp::make_list({});
	date->add(bvd::Sexp::make_atom("date"));
	date->add(bvd::Sexp::make_atom("1998"));
	root->add(std::move(date));

	bvd::SexpTree tree(std::move(root));
	EXPECT_THAT(tree.toString(), testing::StrEq("(\"message\" (\"date\" \"1998\"))"));
}

TEST(TestSexpTreeParser, BasicParsingString)
{
	std::string sexpString = "(\"message\" (\"date\" \"1998\"))";

	bvd::SexpTree tree;
	bool result = tree.parse(sexpString);
	EXPECT_TRUE(result);

	EXPECT_THAT(tree.toString(), testing::StrEq("(\"message\" (\"date\" \"1998\"))"));
}

TEST(TestSexpTreeParser, EmptyString)
{
	std::string sexpString = "";

	bvd::SexpTree tree;
	bool result = tree.parse(sexpString);
	EXPECT_FALSE(result);
}

TEST(TestSexpTreeParser, EmptyList)
{
	std::string sexpString = "()";

	bvd::SexpTree tree;
	bool result = tree.parse(sexpString);
	EXPECT_TRUE(result);

	EXPECT_THAT(tree.toString(), testing::StrEq("()"));

	bvd::sexp_p root = bvd::Sexp::make_list({});
	bvd::SexpTree tree2(std::move(root));
	EXPECT_THAT(tree2.toString(), testing::StrEq("()"));
}

TEST(TestSexpTreeParser, OnlyOneAtom)
{
	std::string sexpString = "\"Test string\"";

	bvd::SexpTree tree;
	bool result = tree.parse(sexpString);
	EXPECT_TRUE(result);

	EXPECT_THAT(tree.toString(), testing::StrEq("\"Test string\""));
}
