
if(BUILD_TESTS)
  set(GTEST_VERSION 1.13.0)
  set(GTEST_PATH "${PROJECT_SOURCE_DIR}/third-party/googletest-${GTEST_VERSION}")
  if(NOT EXISTS ${GTEST_PATH})
    execute_process(
      COMMAND ${CMAKE_COMMAND} -E tar xzf "${GTEST_PATH}.tar.gz"
      WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}/third-party")
  endif()

  FetchContent_Declare(googletest SOURCE_DIR "${GTEST_PATH}")
  FetchContent_MakeAvailable(googletest)
endif()

