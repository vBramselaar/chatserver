var ChatClient = {
	webSocket : null,
	chatboxView : null,
	chatInput : null,

	init : function(address) {
		chatboxView = document.getElementById("chatbox");
		chatInput = document.getElementById("chatInput");
		ChatClient.connect(address)
	},

	connect : function(address) {
		try {
			webSocket = new WebSocket(address);

			webSocket.onopen = function () {
				ChatClient.printText("Go ahead! Start chatting! :)");
			};

			webSocket.onmessage = function (msg) {
				ChatClient.printMessage(msg.data);
			};

			webSocket.onclose = function () {
				ChatClient.printText("Can't chat, no connection :(");
			};
		}
		catch (e) {
			console.error(e, e.stack);
		}
	},

	enterToSend : function(event) {
		if (event.key == "Enter") {
			ChatClient.sendText()
			chatInput.value = ''
		}
	},

	sendText : function() {
		var sendText = chatInput.value.trim();

		if (sendText == "") {
			return;
		}
		else {
			try {
				webSocket.send(sendText);
			}
			catch (e) {
				console.error(e, e.stack);
			}
		}
	},

	printText : function(text) {
		messageElement = document.createElement("p");
		messageElement.innerText = text;
		chatboxView.appendChild(messageElement);
		chatboxView.scrollTop = chatboxView.scrollHeight;
	},

	printMessage : function(text) {
		const elem = document.createElement("p");

		dateElement = document.createElement("span");
		dateElement.className = "message-date";
		dateElement.innerText = text.substring(0, text.indexOf(']') + 1);

		messageElement = document.createElement("span");
		messageElement.className = "message-text";
		messageElement.innerText = text.substring(text.indexOf(']') + 2);

		elem.appendChild(dateElement);
		elem.appendChild(messageElement);

		chatboxView.appendChild(elem);
		chatboxView.scrollTop = chatboxView.scrollHeight;
	},

	selectAll : function() {
		chatInput.select();
	}
};
