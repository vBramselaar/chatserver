#ifndef TIME_HPP
#define TIME_HPP

#include <chrono>
#include <iomanip>

inline std::string getDateTime()
{
	const std::time_t timepoint = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

	std::ostringstream oss;
	oss << std::put_time(std::localtime(&timepoint), "%F %T");
	return oss.str();
}

inline std::string getTimestamp()
{
	const auto	now = std::chrono::system_clock::now();
	const std::time_t timepoint = std::chrono::system_clock::to_time_t(now);
	const auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) % 1000;

	std::ostringstream oss;
	oss << std::put_time(std::localtime(&timepoint), "%F %T") << '.' << std::setfill('0') << std::setw(3) << ms.count();
	return oss.str();
}

#endif
