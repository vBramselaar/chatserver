#ifndef BOOST_WEBSOCKET_HPP
#define BOOST_WEBSOCKET_HPP

#include "connection.hpp"
#include <string>
#include <type_traits>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>

using tcp = boost::asio::ip::tcp;
namespace bvd
{
	class logger;
}

class boost_websocket : public connection<boost_websocket>
{
	friend class connection<boost_websocket>;

public:
	boost_websocket(bvd::logger& logger);

private:
	boost::asio::io_context m_ioc;
	boost::asio::signal_set m_signals;
	tcp::acceptor m_acceptor;
	bvd::logger& m_logger;

	auto bind_impl(std::string_view addr, unsigned short port) -> void;
	auto run_impl() -> void;
	auto stop_impl() -> void;
	auto address_impl() const -> std::string;
	auto port_impl() const -> unsigned short;

	template<typename FuncType>
	auto on_accept_impl(const FuncType& callback) -> void
	{
		static_assert(std::is_invocable_v<FuncType, boost::beast::error_code, tcp::socket>,
		              "Type expected to be callable function");

		m_acceptor.async_accept(boost::asio::make_strand(m_ioc),
		                        callback);
	}
};

#endif
