#ifndef ROOM_H
#define ROOM_H

#include <vector>
#include <memory>
#include <string>
#include <mutex>
#include <deque>
#include <utility>
#include "client.hpp"
#include "logger.hpp"

class Room
{
public:
	Room(bvd::logger& logger);
	void share(const std::string& message);
	void join(std::shared_ptr<Client> client);
	void leave(std::shared_ptr<Client> client);

private:
	std::vector<std::shared_ptr<Client>> m_clients;
	std::mutex m_utex;
	std::deque<std::string> m_messageHistory;
	bvd::logger& m_logger;

	void sendAllClients(const std::string& message);
};

#endif
