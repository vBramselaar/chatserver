#ifndef SERVER_H
#define SERVER_H

#include <boost/asio/ip/tcp.hpp>
#include <memory>
#include <string>
#include "logger.hpp"
#include "room.hpp"
#include "settings.hpp"
#include "connection.hpp"

namespace asio = boost::asio;
using tcp = boost::asio::ip::tcp;

template<typename WebsocketType>
class Server : public std::enable_shared_from_this<Server<WebsocketType>>
{
public:
	Server(const Settings& settings,
	       connection<WebsocketType>& websocket,
	       bvd::logger& logger)
		: m_websocket(websocket)
		, m_settings(settings)
		, m_idCounter(m_settings.startId)
		, m_logger(logger)
	{
		LOG(m_logger, "Server starting up...");
		m_websocket.bind(m_settings.address, m_settings.port);
		m_room = std::make_shared<Room>(m_logger);
		LOG(m_logger, "Server binded to: " << m_websocket.address() << " " << m_websocket.port());
	}

	Server(const Server&) = delete;
	Server& operator=(const Server&) = delete;

	void run()
	{
		LOG(m_logger, "Server starts running");
		do_accept();
		m_websocket.run();
		LOG(m_logger, "Server stopped running");
	}

private:
	connection<WebsocketType>& m_websocket;
	const Settings& m_settings;
	uint64_t m_idCounter;
	std::shared_ptr<Room> m_room;
	bvd::logger& m_logger;

	void do_accept()
	{
		m_websocket.on_accept(beast::bind_front_handler(
			                      &Server::accept_received,
			                      this));
	}

	void accept_received(beast::error_code error,
	                     asio::ip::tcp::socket peer)
	{
		DEBUG(m_logger, "accept_received()");
		if (!error)
		{
			std::string ip = peer.remote_endpoint().address().to_string();
			uint64_t id = m_idCounter++;
			LOG(m_logger, "Client connected from " << ip << " with id \"" << id << "\"");

			std::shared_ptr<Client> client = std::make_shared<Client>(std::move(peer), id, m_logger);
			client->set(m_room);
			client->run();
			m_room->join(client);
		}

		do_accept();
	}
};

#endif
