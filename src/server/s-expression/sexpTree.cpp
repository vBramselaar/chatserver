#include "sexpTree.hpp"
#include <functional>
#include <iostream>

bvd::SexpTree::SexpTree()
	: m_root()
{
}

bvd::SexpTree::SexpTree(sexp_p p)
	: m_root(std::move(p))
{
}

bool bvd::SexpTree::parse(const std::string& expression)
{
	m_root = recursiveParse(expression);
	if (!m_root)
	{
		return false;
	}

	return true;
}

bvd::sexp_p bvd::SexpTree::recursiveParse(const std::string& e)
{
	sexp_p tree;

	for (std::size_t i = 0; i < e.size(); i++)
	{
		char c = e[i];
		if (c == '(')
		{
			std::optional<std::size_t> size = getListLength(i, e);
			if (!size)
			{
				return nullptr;
			}

			if (tree)
			{
				sexp_p child = recursiveParse(e.substr(i, *size));
				if (!child)
				{
					return nullptr;
				}

				tree->add(std::move(child));
				i += *size;
			}
			else
			{
				tree = Sexp::make_list({});
			}
		}
		else if (c == '"')
		{
			std::optional<std::size_t> size = getQuotedLength(i, e);
			if (!size)
			{
				return nullptr;
			}

			sexp_p atom = Sexp::make_atom(e.substr(i + 1, *size));
			if (tree)
			{
				tree->add(std::move(atom));
			}
			else
			{
				tree = std::move(atom);
			}
			i += *size + 1;
		}
	}

	return tree;
}

std::string bvd::SexpTree::toString() const
{
	std::string expression("");

	if (m_root)
	{
		recursiveToString(m_root, expression);
	}

	return expression;
}

void bvd::SexpTree::recursiveToString(const sexp_p& root, std::string& out) const
{
	if (!root)
	{
		return;
	}

	if (std::holds_alternative<atom_t>(**root))
	{
		out += '"' + std::get<atom_t>(**root) + '"';
	}
	else
	{
		const list_t& list = std::get<list_t>(**root);
		out += "(";
		for (const std::unique_ptr<Sexp>& expr : list)
		{
			recursiveToString(expr, out);
			if (expr != list.back())
			{
				out += " ";
			}
		}
		out += ")";
	}
}

const bvd::Sexp& bvd::SexpTree::getChildByPath(const std::string& path) const
{
	return *m_root;
}

std::optional<std::size_t> bvd::SexpTree::getQuotedLength(std::size_t index, const std::string& e)
{
	index++;
	for (std::size_t i = index; i < e.size(); i++)
	{
		if (e[i] == '"')
		{
			return i - index;
		}
	}

	std::cout << "No closing quotation" << std::endl;
	return std::nullopt;
}

std::optional<std::size_t> bvd::SexpTree::getListLength(std::size_t index, const std::string& e)
{
	int listDepth = 0;

	for (std::size_t i = index; i < e.size(); i++)
	{
		if (e[i] == '(')
		{
			listDepth++;
		}
		else if (e[i] == ')')
		{
			listDepth--;
			if (listDepth == 0)
			{
				return i - index + 1;
			}
		}
	}

	if (listDepth < 0)
	{
		std::cout << "Too many closing parenthesis in: " << e.substr(index) << std::endl;
	}
	else if (listDepth > 0)
	{
		std::cout << "Too many opening parenthesis in: " << e.substr(index) << std::endl;
	}

	return std::nullopt;
}
