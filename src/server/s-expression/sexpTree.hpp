#ifndef BVD_SEXPTREE_H
#define BVD_SEXPTREE_H

#include <string>
#include <vector>
#include <memory>
#include <optional>
#include "sexp.hpp"

namespace bvd
{
	class SexpTree
	{
	public:
		SexpTree();
		SexpTree(sexp_p p);
		bool parse(const std::string& expression);
		std::string toString() const;
		const Sexp& getChildByPath(const std::string& path) const;

	private:
		std::unique_ptr<Sexp> m_root;

		sexp_p recursiveParse(const std::string& e);
		void recursiveToString(const sexp_p& root, std::string& out) const;
		std::optional<std::size_t> getQuotedLength(std::size_t index, const std::string& e);
		std::optional<std::size_t> getListLength(std::size_t index, const std::string& e);
	};
}

#endif
