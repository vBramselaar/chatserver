#ifndef BVD_SEXP_H
#define BVD_SEXP_H

#include <string>
#include <vector>
#include <variant>
#include <memory>

namespace bvd
{
	using atom_t = std::string;
	using sexp_p = std::unique_ptr<class Sexp>;
	using list_t = std::vector<sexp_p>;
	using sexp = std::variant<atom_t, list_t>;

	class Sexp
	{
	public:
		explicit Sexp(sexp&& sp)
			: v(std::move(sp))
		{ }

		const std::variant<atom_t, list_t>& operator*() const noexcept
		{
			return v;
		}

		std::variant<atom_t, list_t>& operator*() noexcept
		{
			return v;
		}

		void add(sexp_p sexp)
		{
			if (std::holds_alternative<list_t>(v))
			{
				list_t& list = std::get<list_t>(v);
				list.emplace_back(std::move(sexp));
			}
		}

		static sexp_p make_atom(atom_t&& atom)
		{
			return std::make_unique<bvd::Sexp>(std::move(atom));
		}

		static sexp_p make_list(list_t&& list)
		{
			return std::make_unique<bvd::Sexp>(std::move(list));
		}

	private:
		sexp v;
	};
}

#endif
