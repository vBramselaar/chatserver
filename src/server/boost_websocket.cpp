#include "boost_websocket.hpp"
#include "logger.hpp"
#include <boost/asio/io_service.hpp>

boost_websocket::boost_websocket(bvd::logger& logger)
	: m_ioc{1}
	, m_signals{m_ioc, SIGINT, SIGTERM}
	, m_acceptor(m_ioc)
	, m_logger(logger)
{ }

auto boost_websocket::bind_impl(std::string_view addr, unsigned short port) -> void
{
	boost::beast::error_code ec;

	const auto endpoint = tcp::endpoint{boost::asio::ip::make_address(addr), port};

	m_acceptor.open(endpoint.protocol(), ec);
	if(ec)
	{
		ERROR(m_logger, "Open: " << ec.message());
		return;
	}

	m_acceptor.bind(endpoint, ec);
	if(ec)
	{
		ERROR(m_logger, "Bind: " << ec.message());
		return;
	}

	m_acceptor.listen(boost::asio::socket_base::max_listen_connections, ec);
	if(ec)
	{
		ERROR(m_logger, "Listen: " << ec.message());
		return;
	}
}

auto boost_websocket::run_impl() -> void
{
	m_signals.async_wait(std::bind(&boost::asio::io_service::stop, &m_ioc));
	m_ioc.run();
}

auto boost_websocket::stop_impl() -> void
{
	m_ioc.stop();
}

auto boost_websocket::address_impl() const -> std::string
{
	return m_acceptor.local_endpoint().address().to_string();
}

auto boost_websocket::port_impl() const -> unsigned short
{
	return m_acceptor.local_endpoint().port();
}
