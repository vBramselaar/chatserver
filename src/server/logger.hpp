#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <ostream>
#include <iostream>
#include <fstream>
#include <memory>
#include <deque>
#include <filesystem>
#include <mutex>
#include "time.hpp"

#ifdef _MSC_VER
#define __FILENAME__ (strrchr("\\" __FILE__, '\\') + 1)
#else
#define __FILENAME__ (strrchr("/" __FILE__, '/') + 1)
#endif

#define LOG(LOGGER, ARG) {std::lock_guard l(LOGGER); LOGGER.info() << "(" << __FILENAME__ << ":" << __LINE__ << ") " << ARG << '\n';}
#define WARNING(LOGGER, ARG) {std::lock_guard l(LOGGER); LOGGER.warning() << "(" << __FILENAME__ << ":" << __LINE__ << ") " << ARG << '\n';}
#define ERROR(LOGGER, ARG) {std::lock_guard l(LOGGER); LOGGER.error() << "(" << __FILENAME__ << ":" << __LINE__ << ") " << ARG << '\n';}
#ifdef DEBUG_MODE
#define DEBUG(LOGGER, ARG) {std::lock_guard l(LOGGER); LOGGER.debug() << "(" << __FILENAME__ << ":" << __LINE__ << ") " << ARG << '\n';}
#else
#define DEBUG(LOGGER, ARG) do{}while(0)
#endif

#define SIZE_PER_FILE (20 * 1000000)

namespace bvd
{
	class logger
	{
	public:
		virtual ~logger() {};
		virtual std::ostream& debug() = 0;
		virtual std::ostream& info() = 0;
		virtual std::ostream& warning() = 0;
		virtual std::ostream& error() = 0;
		virtual void lock() = 0;
		virtual void unlock() = 0;
	};

	class console_logger : public logger
	{
	public:
		console_logger() = default;
		console_logger(const console_logger&) = delete;
		console_logger& operator=(const console_logger&) = delete;

		~console_logger()
		{
			std::cout << "\033[0m";
			std::cerr << "\033[0m";
		}

		std::ostream& debug() override
		{
			std::cout << "\033[0;34m" << "DEBUG> [" << getTimestamp() << "] ";
			return std::cout;
		}

		std::ostream& info() override
		{
			std::cout << "\033[0m" <<    "INFO>  [" << getTimestamp() << "] ";
			return std::cout;
		}

		std::ostream& warning() override
		{
			std::cerr << "\033[0;33m" << "WARN>  [" << getTimestamp() << "] ";
			return std::cerr;
		}

		std::ostream& error() override
		{
			std::cerr << "\033[0;31m" << "ERROR> [" << getTimestamp() << "] ";
			return std::cerr;
		}

		void lock() override
		{
			mx.lock();
		}

		void unlock() override
		{
			mx .unlock();
		}

	private:
		std::mutex mx;
	};

	class file_logger : public logger
	{
	public:
		file_logger(std::string_view name, std::size_t max_logfiles_on_disk = 2)
			: m_name(name)
			, m_max_logfiles_on_disk(max_logfiles_on_disk)
			, m_written_logfiles({m_name + getTimestamp() + ".log"})
			, m_filestream(m_written_logfiles.back(), std::ios_base::app)
		{
		}

		file_logger(const file_logger&) = delete;
		file_logger& operator=(const file_logger&) = delete;
		~file_logger() = default;

		std::ostream& debug() override
		{
			if (file_exceeds_max_size())
			{
				switch_to_new_logfile();
			}

			m_filestream << "DEBUG> [" << getTimestamp() << "] ";
			return m_filestream;
		}

		std::ostream& info() override
		{
			if (file_exceeds_max_size())
			{
				switch_to_new_logfile();
			}

			m_filestream << "INFO> [" << getTimestamp() << "] ";
			return m_filestream;
		}

		std::ostream& warning() override
		{
			if (file_exceeds_max_size())
			{
				switch_to_new_logfile();
			}

			m_filestream << "WARN> [" << getTimestamp() << "] ";
			return m_filestream;
		}

		std::ostream& error() override
		{
			if (file_exceeds_max_size())
			{
				switch_to_new_logfile();
			}

			m_filestream << "ERROR> [" << getTimestamp() << "] ";
			return m_filestream;
		}

		void lock() override
		{
			mx.lock();
		}

		void unlock() override
		{
			mx .unlock();
		}

	private:
		std::string m_name;
		std::size_t m_max_logfiles_on_disk;
		std::deque<std::string> m_written_logfiles;
		std::ofstream m_filestream;
		std::mutex mx;

		bool file_exceeds_max_size()
		{
			std::uintmax_t size = std::filesystem::file_size(m_written_logfiles.back());
			return (size > SIZE_PER_FILE);
		}

		void switch_to_new_logfile()
		{
			m_written_logfiles.emplace_back(m_name + getTimestamp() + ".log");
			if (m_written_logfiles.size() > m_max_logfiles_on_disk)
			{
				std::filesystem::remove(m_written_logfiles.front());
				m_written_logfiles.pop_front();
			}

			m_filestream = std::ofstream(m_written_logfiles.back(), std::ios_base::app);
		}
	};
}

#endif
