#include "room.hpp"
#include "client.hpp"
#include "time.hpp"
#include <iostream>

Room::Room(bvd::logger& logger)
	: m_logger(logger)
{ }

void Room::share(const std::string& message)
{
	std::lock_guard<std::mutex> guard(m_utex);
	std::string messageWithTime = "[" + getDateTime() + "]" + " " + message;
	sendAllClients(messageWithTime);
}

void Room::sendAllClients(const std::string& message)
{
	std::vector<std::shared_ptr<Client>> toBeDeleted;
	for (auto client : m_clients)
	{
		if (client->isOpen())
		{
			client->send(message);
		}
		else
		{
			toBeDeleted.emplace_back(client);
		}
	}

	for (auto client : toBeDeleted)
	{
		LOG(m_logger, "Client closed");
		m_clients.erase(std::remove(m_clients.begin(), m_clients.end(), client), m_clients.end());
	}

	m_messageHistory.emplace_back(message);
	if (m_messageHistory.size() > 20)
	{
		m_messageHistory.pop_front();
	}
}

void Room::join(std::shared_ptr<Client> client)
{
	std::lock_guard<std::mutex> guard(m_utex);
	m_clients.emplace_back(client);

	for (const std::string& message : m_messageHistory)
	{
		client->send(message);
	}

	LOG(m_logger, "Amount of clients: " << m_clients.size());
}

void Room::leave(std::shared_ptr<Client> client)
{
	std::lock_guard<std::mutex> guard(m_utex);
	LOG(m_logger, "Client " << client->getId() << " is leaving room with " << m_clients.size() << " clients");
	m_clients.erase(std::remove(m_clients.begin(), m_clients.end(), client), m_clients.end());
	LOG(m_logger, "Now only " << m_clients.size() << " clients left");
}
