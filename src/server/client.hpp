#ifndef CLIENT_H
#define CLIENT_H

#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <thread>
#include <memory>
#include "logger.hpp"

namespace beast = boost::beast;
namespace asio = boost::asio;
using websocket = beast::websocket::stream<asio::ip::tcp::socket>;

class Room;

class Client : public std::enable_shared_from_this<Client>
{
public:
	Client(asio::ip::tcp::socket socket, uint64_t id, bvd::logger& logger);
	~Client();
	void run();
	void send(const std::string& message);
	void set(std::weak_ptr<Room> room);
	bool isOpen() const { return m_websocket->is_open(); }
	const std::string& getUsername() const { return m_username; }
	uint64_t getId() const { return m_id; }

private:
	std::shared_ptr<websocket> m_websocket;
	uint64_t m_id;
	std::weak_ptr<Room> m_room;
	beast::multi_buffer buffer;
	std::string m_username;
	bvd::logger& m_logger;

	void read();
	void readHandler(beast::error_code const& ec, std::size_t bytes_transferred);
};

#endif
