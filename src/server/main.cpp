#include "server.hpp"
#include "settings.hpp"
#include "settingsReader.hpp"
#include "logger.hpp"
#include "boost_websocket.hpp"

int main(int argc, char* argv[])
{
	bvd::console_logger init_logger;

	LOG(init_logger, "Trying to read config file...");

	Settings settings;
	bool configfileIsPresent = std::filesystem::exists("defaults.conf");
	if (configfileIsPresent)
	{
		SettingsReader::parse("defaults.conf", settings);
		LOG(init_logger, "Read config file named 'defaults.conf'");
	}
	else
	{
		WARNING(init_logger, "No config file read, using default values");
	}

	std::unique_ptr<bvd::logger> logger;
	if (settings.log)
	{
		LOG(init_logger, "Will continue logging to file");
		logger = std::make_unique<bvd::file_logger>("chatserver");
	}
	else
	{
		logger = std::make_unique<bvd::console_logger>();
	}

	if (argc == 3)
	{
		settings.address = argv[1];
		settings.port = static_cast<unsigned short>(std::atoi(argv[2]));
	}

	auto websocket = boost_websocket{*logger};
	auto server = Server{settings, websocket, *logger};
	server.run();
}
