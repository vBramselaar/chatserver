#ifndef CONNECTION_HPP
#define CONNECTION_HPP

#include <string>

template<typename T>
class connection
{
public:
	auto bind(std::string_view addr, unsigned short port) -> void
	{
		self().bind_impl(addr, port);
	}

	template<typename FuncType>
	auto on_accept(const FuncType& callback)
	{
		self().on_accept_impl(callback);
	}

	auto run() -> void
	{
		self().run_impl();
	}

	auto stop() -> void
	{
		self().stop_impl();
	}

	auto address() const -> std::string
	{
		return self().address_impl();
	}

	auto port() const -> unsigned short
	{
		return self().port_impl();
	}

protected:
	connection()
	{ }

private:
	auto self() -> T& { return static_cast<T&>(*this); }
	auto self() const -> const T& { return static_cast<T const&>(*this); }
};

#endif
