#include "client.hpp"
#include "room.hpp"
#include <chrono>

Client::Client(asio::ip::tcp::socket socket, uint64_t id, bvd::logger& logger)
	: m_id(id)
	, m_username("Anonymous " + std::to_string(m_id))
	, m_logger(logger)
{
	try
	{
		m_websocket = std::make_shared<websocket>(std::move(socket));
		m_websocket->set_option(beast::websocket::stream_base::decorator(
			                        [](beast::websocket::response_type& res)
			                        {
				                        res.set(beast::http::field::server,
				                                std::string(BOOST_BEAST_VERSION_STRING) + " Server");
			                        }));

		beast::websocket::stream_base::timeout opt;
		opt.keep_alive_pings = true;
		opt.idle_timeout = std::chrono::seconds(20);
		opt.handshake_timeout = std::chrono::seconds(20);
		m_websocket->set_option(opt);

		m_websocket->accept();
	}
	catch(std::exception const& e)
	{
		ERROR(m_logger, e.what());
	}
}

Client::~Client()
{ }

void Client::run()
{
	read();
}

void Client::send(const std::string& message)
{
	asio::const_buffer b(message.c_str(), message.length());
	m_websocket->text(true);
	m_websocket->write(b);
}

void Client::set(std::weak_ptr<Room> room)
{
	m_room = room;
}

void Client::read()
{
	m_websocket->async_read(buffer, beast::bind_front_handler(
		                        &Client::readHandler, shared_from_this()));
	DEBUG(m_logger, "async_read()");
}

void Client::readHandler(beast::error_code const& ec, std::size_t bytes_transferred)
{
	boost::ignore_unused(bytes_transferred);
	DEBUG(m_logger, "readHandler()");

	if (!ec)
	{
		if (m_websocket->got_text())
		{
			if (std::shared_ptr<Room> spt = m_room.lock())
			{
				std::string message = m_username + ": " + beast::buffers_to_string(buffer.data());
				DEBUG(m_logger, "Sending: \"" << message << "\"");
				spt->share(message);
			}
			else
			{
				ERROR(m_logger, "No room assigned to client");
			}
		}

		buffer.consume(buffer.size());
		read();
	}
	else
	{
		if(ec == beast::websocket::error::closed)
		{
			DEBUG(m_logger, "Client " << m_id << " closed");
		}
		else
		{
			ERROR(m_logger, ec.message());
		}

		if (std::shared_ptr<Room> spt = m_room.lock())
		{
			spt->leave(shared_from_this());
		}
	}
}
