#ifndef SETTINGSREADER_H
#define SETTINGSREADER_H

#include <string>
#include <sstream>
#include <type_traits>
#include <utility>
#include <functional>
#include "settings.hpp"

using confsetting_t = std::pair<std::string, std::string>;

class SettingsReader
{
public:
	static void parse(const std::string& filename, Settings& settings);

private:
	static void handleSettingBy(const std::string& filename, std::function<void(const confsetting_t&)> handler);

	template<typename T>
	static void setValue(T& setting, const std::string& value)
	{
		std::istringstream iss(value);
		if (std::is_same<T, bool>::value)
		{
			iss >> std::boolalpha >> setting;
		}
		else
		{
			iss >> setting;
		}
	}
};

#endif
