#include "settingsReader.hpp"
#include <fstream>
#include <stdexcept>

void SettingsReader::parse(const std::string& filename, Settings& settings)
{
	handleSettingBy(filename, [&settings](const confsetting_t& setting)
	{
		if (setting.first == "address")
		{
			setValue(settings.address, setting.second);
		}
		else if (setting.first == "port")
		{
			setValue(settings.port, setting.second);
		}
		else if (setting.first == "log")
		{
			setValue(settings.log, setting.second);
		}
		else if (setting.first == "messageMaxSize")
		{
			setValue(settings.messageMaxSize, setting.second);
		}
		else if (setting.first == "rateLimitMs")
		{
			setValue(settings.rateLimitMs, setting.second);
		}
		else if (setting.first == "startId")
		{
			setValue(settings.startId, setting.second);
		}
		else
		{
			throw std::invalid_argument("Unknown setting \"" + setting.first + "\"");
		}
	});
}

void SettingsReader::handleSettingBy(const std::string& filename, std::function<void(const confsetting_t&)> handler)
{
	std::ifstream file(filename);
	std::string line;
	while (std::getline(file, line))
	{
		std::size_t pos = line.find_first_of('=');
		if (pos != std::string::npos)
		{
			confsetting_t setting;
			setting.first = line.substr(0, pos);
			setting.second = line.substr(pos + 1);

			handler(setting);
		}
	}
}
