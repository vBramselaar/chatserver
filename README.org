
* Chatserver
  A simple websocket based chatserver I made. As of writing, no real
  protocol is implemented. The server now just sends the text from one
  client to all the others and prepends a timestamp to the message.

** Building
*** Dependencies
    - C++17
    - Boost 1.74 >=

*** Commands
   #+begin_src shell
cmake --preset=debug
cmake --build --preset=debug
   #+end_src

   For building a Debian package run:
   #+begin_src shell
dpkg-buildpackage -B
   #+end_src

   After installing the .deb file, you can view the logs from the
   server with:
   #+begin_src shell
sudo journalctl -f -u chatserver.service
   #+end_src

** Usage
*** The server
   At startup of the server, it tries to read the defaults.conf from
   its working directory. This is a simple ini config (option=value
   format) with the possibility to configure the following options:

   | Option         | Value                                                 | Defaults |
   |----------------+-------------------------------------------------------+----------|
   | address        | Ip address on which the server binds itself.          |  0.0.0.0 |
   | port           | Port on which the server listens.                     |     8080 |
   | log            | To enable logging to file (instead of stdout).        |    false |
   | messageMaxSize | Max size of client messages in characters.            |      200 |
   | rateLimitMs    | How fast a client might spam messages in ms.          |       10 |
   | startId        | From which number the clients get their anonymous id. |        1 |

*** The client
    In the directory ~src/html~ I created an example client to use as
    chat on your website.  You can already test it by just locally
    opening ~websocket.html~ in your browser when running the server on
    localhost.
